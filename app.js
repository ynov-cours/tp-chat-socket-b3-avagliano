const express = require('express');
const http = require('http');
const app = express();

// Chargement du fichier index.html affiché au client
const server = http.createServer(app);
// On envoie le fichier html :)
app.use(express.static(__dirname + '/public'));

// Chargement de socket.io
let io = require("socket.io").listen(server);
let users = [];
let connections = [];
let messages = [];

io.on("connection", function (socket) {
  connections.push(socket);
  console.log("Nouvelle connexion : %s sockets connectés", connections.length);
  io.emit('connection message', '[Info] Un utilisateur s\'est connecté');
  io.emit('userNumber', "1");

  socket.on("disconnect", function(data){
    connections.pop(connections.indexOf(socket), 1);
    console.log("Nouvelle déconnexion : %s socket conectés", connections.length);
    io.emit('connection message', '[Info] Un utilisateur s\'est déconecté');
    io.emit('userNumber', connections.length);
  });

  //Send Message
  socket.on("send", function(data){
    console.log(data);
    io.sockets.emit('new message', {msg: data});
  });
});

// function getCookie(c_name) {
//   if (document.cookie.length > 0) {
//     c_start = document.cookie.indexOf(c_name + "=");
//     if (c_start != -1) {
//       c_start = c_start + c_name.length + 1;
//       c_end = document.cookie.indexOf(";", c_start);
//       if (c_end == -1) {
//         c_end = document.cookie.length;
//       }
//       return unescape(document.cookie.substring(c_start, c_end));
//     }
//   }
//   return "";
// }

server.listen(3000);

// module.exports = {users, connections};
