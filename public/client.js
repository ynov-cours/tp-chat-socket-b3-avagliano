const socket = io.connect('http://localhost:3000');
const messageForm = $('#messageForm');
const message = $('#message');
const chat = $('#chat');
const infos = $('#label-infos');

// document.getElementById('chat').innerHTML = getCookie(message);

messageForm.submit(function(e){
  e.preventDefault();
  socket.emit('send', message.val());
  message.val('');
  // document.cookie = `message=${message.val()}; expires=Sun, 1 Jan 2023 00:00:00 UTC; path=/`
});

socket.on("new message", function(data){
  chat.append("<div>" + data.msg + "</div>");
});

socket.on('connection message', (args) => {
  chat.append("<div>" + args + "</div>");
})

socket.on('userNumber', (connections) => {
  infos.innerHTML = "Votre message - " + connections + "utilisateurs présents";
})
